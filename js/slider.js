var SLIDER = (function() {
  var slider = document.querySelector(".js-slider");
  var slides = document.querySelector(".js-slides");
  var slide = document.querySelectorAll(".js-slide");
  var controlsLeft = document.querySelector(".js-slider-controls-left");
  var controlsRight = document.querySelector(".js-slider-controls-right");
  var cookieDetails = document.querySelector(".cookie-details");
  var position = 0;

  function sliderHandler(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    if(ev.target === controlsRight) {
      if(position === -200) {
        slides.style.left = "0";
        position = 0;
      } else {
        slides.style.left = position-100 + "%";
        position = parseInt(slides.style.left);
      }
    }else if(ev.target === controlsLeft) {
      if(position === 0) {
        slides.style.left = "-200%";
        position = -200;
      } else {
        slides.style.left = position+100 + "%";
        position = parseInt(slides.style.left);
      }
    } else if (ev.target.className === "js-slide"){
      EVT.emit("cookie-clicked", "partials/cookies/" + ev.target.dataset.name + ".html" , cookieDetails);
    }
  }
  
  function init(){
    slider.addEventListener('click', sliderHandler, false);
  }

  return {
    init: init
  }
})();
