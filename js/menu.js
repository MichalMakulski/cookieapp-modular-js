var MENU = (function() {
  var menuToggle = document.querySelector(".js-toggle-menu");
  var navContainer = document.querySelector(".js-nav-container");
  var contact = document.querySelector(".js-contact"); 
  var about = document.querySelector(".js-about"); 
  var menuContent = document.querySelector(".js-menu-content");

  navContainer.opened = false;

  function toggleMenu(ev) {
    if(ev.target === menuToggle){
      if(navContainer.opened){
        navContainer.style.right = "-300px";  
      }else {
        navContainer.style.right = "0";
      } 
      navContainer.opened = !navContainer.opened;
    }              
  }

  function clickedMenuItem(ev){
    if(ev.target.className === "js-menu-item"){
      EVT.emit("menu-item-clicked", "partials/" + ev.target.dataset.name + ".html" , menuContent);
    }
  }

  function init(){
    navContainer.addEventListener('click', toggleMenu, false);
    navContainer.addEventListener('click', clickedMenuItem, false);
  }

  return {
    init: init
  }
})();
