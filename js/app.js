(function(public){

	window.EVT = new EventEmitter2();

	public.ver = "1.0";

	public.init = function(){
		AJAX.init();
		SLIDER.init();
		MENU.init();
	};

})(this.APP = {});

window.addEventListener('load', APP.init, false);

