var EVT = new EventEmitter2();

var AJAX = (function() {

  var xmlhttp, data;

  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

  function load(src, dest) {
    xmlhttp.onreadystatechange = function() {
      data = xmlhttp.responseText;
      dest.innerHTML = data;
    };
    xmlhttp.open("GET", src, true);
    xmlhttp.send();
  }
  
  function init(){
    EVT.on("btn-clicked", load);
  }
  
  return {
    init: init
  }
  
})();


var DETAILS = (function() {
  var menu = document.getElementById('menu');
  var div = document.querySelector("#mydiv");

  function init() {
    menu.addEventListener('click',function(ev){
       ev.preventDefault();
       ev.stopPropagation();
      if(ev.target.dataset.name === "btn"){
       EVT.emit("btn-clicked", "http://codepen.io/anon/pen/ojZxmO.html", div);
      }
    }, false); 
  }
  return {
    init: init
  }
})();

//APP init:
AJAX.init();
DETAILS.init();