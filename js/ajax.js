var AJAX = (function() {
  var xmlhttp, data;
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else {
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  function load(src, dest) {
    xmlhttp.onreadystatechange = function() {
      data = xmlhttp.responseText;
      dest.innerHTML = data;
    };
    xmlhttp.open("GET", src, true);
    xmlhttp.send();
  }
  function init(){
    EVT.on("cookie-clicked", load);
    EVT.on("menu-item-clicked", load);
  }
  return {
    init: init
  }  
})();